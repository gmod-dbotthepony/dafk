
-- Copyright (C) 2016-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local EFFECTS =        CreateConVar('cl_dafk_zzz',     '1', FCVAR_ARCHIVE, 'Draw ZZZ chars on the top of sleeping player')
local DISPLAY =        CreateConVar('cl_dafk_screen',  '1', FCVAR_ARCHIVE, 'Draw AFK time on screen')
local DISPLAY_CHAT =   CreateConVar('cl_dafk_chat',    '1', FCVAR_ARCHIVE, 'Display AFK messages in chat if server is allowing to do it.')
local TEXT =           CreateConVar('cl_dafk_text',    '1', FCVAR_ARCHIVE, 'Draw text on the top of sleeping player')
local CAMERA_HIDE =    CreateConVar('cl_dafk_hide',    '1', FCVAR_ARCHIVE, 'Stop drawing AFK effects when camera is deployed')
local FOCUS =          CreateConVar('cl_dafk_focus',   '1', FCVAR_ARCHIVE, 'Broadcast message whatever your game window is focused')

local net = DLib.Net
local SysTime = SysTime

DLib.RegisterAddonName('DAFK')

local freezeTimer = false
local freezeTime = 0
local fadeAt = 0

local function Receive()
	local ply = net.ReadEntity()
	local status = net.ReadBool()
	if not IsValid(ply) then return end

	local time

	if not status then
		time = net.ReadUInt(32)

		if ply == LocalPlayer() then
			freezeTimer = true
			fadeAt = SysTime() + 6
			freezeTime = time
		end
	end

	local message = ply:GenerateAFKMessage(status, time)

	if DISPLAY_CHAT:GetBool() and DAFK_SHOWNOTIFICATIONS:GetBool() then
		if DAFK_NOTIFYRADIUS:GetInt() <= 0 or ply:GetPos():Distance(LocalPlayer():GetPos()) <= DAFK_NOTIFYRADIUS:GetInt() then
			chat.AddText(unpack(message))
		else
			MsgC(unpack(message))
			MsgC('\n')
		end
	else
		MsgC(unpack(message))
		MsgC('\n')
	end
end

surface.CreateFont('DAFK.Text', {
	size = 40,
	font = 'Roboto',
	weight = 600,
	extended = true,
})

surface.CreateFont('DAFK.Zzz', {
	size = 72,
	font = 'Roboto',
	weight = 800,
})

surface.CreateFont('DAFK.Away', {
	size = 100,
	font = 'Roboto',
	weight = 500,
	extended = true,
})

local function GetEyePos(ply)
	local eyePos
	local EYES = ply:LookupAttachment('eyes')

	if EYES and EYES ~= 0 then
		local vec = ply:GetAttachment(EYES)
		eyePos = vec.Pos
		eyePos.z = eyePos.z + 40
	else
		eyePos = ply:EyePos()
		eyePos.z = eyePos.z + 40
	end

	return eyePos
end

local MAX_DISTANCE = 256
local FADEOUT_START = 176

local surface = surface
local GetHumans = player.GetHumans
local LocalPlayer = LocalPlayer

local drawn_players = {}
local drawn_players_i = 0

local function PreRender()
	for i = drawn_players_i, 1, -1 do
		drawn_players[i] = nil
	end

	drawn_players_i = 0
end

local function PostPlayerDraw(ply)
	for i = drawn_players_i, 1, -1 do
		if drawn_players[i] == ply then return end
	end

	drawn_players_i = drawn_players_i + 1
	drawn_players[drawn_players_i] = ply
end

local ZZZToDraw = {}
local next_zzz = 0
local zzz_count = 0
local EyePos = EyePos
local RealFrameTime = RealFrameTime
local random = math.random

local function PostDrawTranslucentRenderables(a, b)
	if a or b then return end

	if CAMERA_HIDE:GetBool() then
		local can = hook.Run('HUDShouldDraw', 'CHudGMod')
		if can == false then return end
	end

	local lply = LocalPlayer()
	local eyes = EyePos()
	local aim = EyeAngles():Forward()

	surface.SetTextColor(255, 255, 255)

	if TEXT:GetBool() then
		surface.SetFont('DAFK.Text')

		for i = 1, drawn_players_i do
			local ply = drawn_players[i]

			if ply ~= lply then
				local distance = ply:GetPos():Distance(eyes)

				if distance < MAX_DISTANCE then
					local eyePos = GetEyePos(ply)
					eyePos.z = eyePos.z - 20

					local dot = eyePos - eyes

					if lply:ShouldDrawLocalPlayer() then
						dot = 1
					else
						dot:Normalize()
						dot = dot:Dot(aim)
					end

					if dot > 0 then
						surface.SetTextColor(255, 255, 255, (1 - distance:progression(FADEOUT_START, MAX_DISTANCE)) * 255 * dot:progression(0.85, 0.96))

						local is_afk = ply:IsAFK()
						local is_tabbed_out = ply:IsTabbedOut()

						if is_afk or is_tabbed_out then
							local text = is_afk and DLib.I18n.Localize('player.dafk.status.afk', DLib.I18n.FormatTime(ply:GetAFKTime())) or
													DLib.I18n.Localize('player.dafk.status.tabbed')

							local delta = (eyePos - eyes):Angle()
							local ang = Angle(0, delta.y - 90, 90)
							local add = Vector(surface.GetTextSize(text) * .05, 0, 0)
							add:Rotate(ang)

							cam.Start3D2D(eyePos - add, ang, 0.1)
							surface.SetTextPos(0, 0)
							surface.DrawText(text)
							cam.End3D2D()
						end
					end
				end
			end
		end
	end

	if EFFECTS:GetBool() then
		local time = SysTime()

		if next_zzz < time then
			if zzz_count == 0 then
				zzz_count = 4
				next_zzz = time + 4
			else
				zzz_count = zzz_count - 1
				next_zzz = time + math.random(25, 60) / 100

				for i = 1, #drawn_players do
					local ply = drawn_players[i]

					if ply ~= lply and ply:IsAFK() then
						local data = {}
						data.pos = GetEyePos(ply)
						data.pos.z = data.pos.z - 20
						data.vel = VectorRand() * 5
						data.vel.z = 4
						data.fade = time + 3

						ZZZToDraw[#ZZZToDraw + 1] = data
					end
				end
			end
		end

		surface.SetFont('DAFK.Zzz')

		for i = #ZZZToDraw, 1, -1 do
			local data = ZZZToDraw[i]

			if data.fade < time then
				table.remove(ZZZToDraw, i)
			else
				local percent = math.min((data.fade - time) / 1.5, 1)
				local percent2 = math.min((data.fade - time) / 3, 1)

				data.vel.z = data.vel.z + RealFrameTime() * random() * 3
				data.vel.x = data.vel.x * .95 + random() - 0.5
				data.vel.y = data.vel.y * .95 + random() - 0.5

				data.pos = data.pos + data.vel * RealFrameTime() * 1.5

				surface.SetTextColor(255, 255, 255, percent * 255)
				local ang1 = (data.pos - eyes):Angle()
				local ang = Angle(0, ang1.y - 90, 90)

				cam.Start3D2D(data.pos, ang, (.3 - percent2 * .3))
				surface.SetTextPos(0, 0)
				surface.DrawText('Z')
				cam.End3D2D()
			end
		end
	end
end

local function PostDrawHUD()
	if not DISPLAY:GetBool() or not freezeTimer and not LocalPlayer():IsAFK() then return end

	if pace and pace.IsActive() then return end
	local time = freezeTimer and freezeTime or LocalPlayer():GetAFKTime()
	local w, h = ScrWL(), ScrHL()

	surface.SetFont('DAFK.Away')
	local percent = (fadeAt - SysTime()) / 6

	if not freezeTimer then
		surface.SetTextColor(255, 255, 255)
		surface.SetDrawColor(0, 0, 0, 150)
	else
		surface.SetTextColor(255, 255, 255, percent * 255)
		surface.SetDrawColor(0, 0, 0, 150 * percent)

		if percent <= 0 then
			freezeTimer = false
			return
		end
	end

	local x, y = w / 2, 200
	local str = DLib.i18n.tformat(time)
	local awayfor = DLib.i18n.localize('player.dafk.status.awayfor')

	local tX, tY = surface.GetTextSize(awayfor)
	local tX2, tY2 = surface.GetTextSize(str)

	render.PushFilterMag(TEXFILTER.ANISOTROPIC)
	render.PushFilterMin(TEXFILTER.ANISOTROPIC)

	surface.DrawRect(x - math.max(tX, tX2) / 2 - 20, y - 20, math.max(tX, tX2) + 40, tY * 2 + 30)

	surface.SetTextPos(x - tX / 2, y)
	surface.DrawText(awayfor)

	surface.SetTextPos(x - tX2 / 2, y + tY + 10)
	surface.DrawText(str)

	render.PopFilterMag()
	render.PopFilterMin()
end

local Think

do
	local LastMouseBeat = 0
	local LastHasFocus
	local system_HasFocus = system.HasFocus

	function Think()
		local focus = not FOCUS:GetBool() or system_HasFocus()

		if LastHasFocus ~= focus then
			net.Start('DAFK.HasFocus')
			net.WriteBool(focus)
			net.SendToServer()
			LastHasFocus = focus
		end

		if
			focus and
			LastMouseBeat < SysTime() and
			(input.IsMouseDown(MOUSE_LEFT) or
			input.IsMouseDown(MOUSE_RIGHT) or
			input.IsMouseDown(MOUSE_MIDDLE))
		then
			LastMouseBeat = SysTime() + 1
			net.Start('DAFK.Heartbeat')
			net.SendToServer()
		end
	end
end

local function PopulateClient(Panel)
	if not IsValid(Panel) then return end
	Panel:Clear()

	local lab = Label('gui.dafk.menu.about')
	Panel:AddItem(lab)
	lab:SetDark(true)

	Panel:CheckBox('gui.dafk.menu.cl_dafk_zzz', 'cl_dafk_zzz')
	Panel:CheckBox('gui.dafk.menu.cl_dafk_screen', 'cl_dafk_screen')
	Panel:CheckBox('gui.dafk.menu.cl_dafk_chat', 'cl_dafk_chat')
	Panel:CheckBox('gui.dafk.menu.cl_dafk_text', 'cl_dafk_text')
	Panel:CheckBox('gui.dafk.menu.cl_dafk_hide', 'cl_dafk_hide')
	Panel:CheckBox('gui.dafk.menu.cl_dafk_focus', 'cl_dafk_focus'):SizeToContents()

	local button = Panel:Button('Steam Workshop')
	button.DoClick = function()
		gui.OpenURL('steamcommunity.com/sharedfiles/filedetails/?id=768912833')
	end

	local button = Panel:Button('GitLab')
	button.DoClick = function()
		gui.OpenURL('https://gitlab.com/DBotThePony/DBotProjects')
	end
end

do
	local Hooks = {
		'StartChat',
		'FinishChat',
		'OnChatTab',
		'KeyPress',
		'ScoreboardShow',
		'ScoreboardHide',
		'PlayerBindPress',
	}

	local last = 0

	local function HookFunc()
		if last > SysTime() then return end
		last = SysTime() + 1
		net.Start('DAFK.Heartbeat')
		net.SendToServer()
	end

	for k, v in ipairs(Hooks) do
		hook.Add(v, 'DAFK.Heartbeat', HookFunc)
	end
end

net.Receive('DAFK.StatusChanges', Receive)

hook.Add('PreRender', 'DAFK Clear Renderables', PreRender)
hook.Add('PostPlayerDraw', 'DAFK Push Renderables', PostPlayerDraw, 10)

hook.Add('PostDrawTranslucentRenderables', 'DAFK.Hooks', PostDrawTranslucentRenderables, 4)
hook.Add('PostDrawHUD', 'DAFK.Hooks', PostDrawHUD)
hook.Add('Think', 'DAFK.Hooks', Think)
hook.Add('PopulateToolMenu', 'DAFK.Hooks', function()
	spawnmenu.AddToolMenuOption('Utilities', 'User', 'DAFK', 'DAFK', '', '', PopulateClient)
end)

timer.Remove('DAFK.ZZZ')
